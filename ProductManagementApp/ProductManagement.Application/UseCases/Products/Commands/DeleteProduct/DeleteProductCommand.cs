﻿using ProductManagement.Application.Messaging;

namespace ProductManagement.Application.UseCases.Products.Commands.DeleteProduct;
public class DeleteProductCommand : ICommand<bool>
{
    public Guid Id { get; set; }

    public DeleteProductCommand(Guid id)
    {
        Id = id;
    }
}
