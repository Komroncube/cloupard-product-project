﻿using Microsoft.EntityFrameworkCore;
using ProductManagement.Application.Contracts;
using ProductManagement.Application.Messaging;

namespace ProductManagement.Application.UseCases.Products.Commands.DeleteProduct;
public class DeleteProductCommandHandler : ICommandHandler<DeleteProductCommand, bool>
{
    private readonly IProductTestDbContext _dbContext;

    public DeleteProductCommandHandler(IProductTestDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<bool> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
    {
        var product = await _dbContext.Products.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        if (product is null)
        {
            throw new Exception("product not found");
        }
        _dbContext.Products.Remove(product);
        int result = await _dbContext.SaveChangesAsync(cancellationToken);
        return result != 0;
    }
}
