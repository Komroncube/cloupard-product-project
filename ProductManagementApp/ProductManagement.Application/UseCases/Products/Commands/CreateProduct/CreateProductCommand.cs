﻿using ProductManagement.Application.DTOs;
using ProductManagement.Application.Messaging;

namespace ProductManagement.Application.UseCases.Products.Commands.CreateProduct;
public class CreateProductCommand : ICommand<bool>
{
    public ProductDTO ProductDto { get; set; }

    public CreateProductCommand(ProductDTO productDto)
    {
        ProductDto = productDto;
    }
}
