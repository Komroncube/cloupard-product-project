﻿using ProductManagement.Application.Contracts;
using ProductManagement.Application.Messaging;
using ProductManagement.Domain.Models;

namespace ProductManagement.Application.UseCases.Products.Commands.CreateProduct;
public class CreateProductCommandHandler : ICommandHandler<CreateProductCommand, bool>
{
    private readonly IProductTestDbContext _dbContext;

    public CreateProductCommandHandler(IProductTestDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<bool> Handle(CreateProductCommand request, CancellationToken cancellationToken)
    {
        var product = new Product()
        {
            Name = request.ProductDto.Name,
            Description = request.ProductDto.Description,
        };
        _dbContext.Products.Add(product);
        int result = await _dbContext.SaveChangesAsync(cancellationToken);
        return result != 0;
    }
}
