﻿using ProductManagement.Application.DTOs;
using ProductManagement.Application.Messaging;

namespace ProductManagement.Application.UseCases.Products.Commands.UpdateProduct;

public class UpdateProductCommand : ICommand<bool>
{
    public Guid Id { get; set; }
    public ProductDTO ProductDto { get; set; }

    public UpdateProductCommand(Guid id, ProductDTO productDto)
    {
        Id = id;
        ProductDto = productDto;
    }
}
