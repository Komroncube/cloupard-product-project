﻿using Microsoft.EntityFrameworkCore;
using ProductManagement.Application.Contracts;
using ProductManagement.Application.Messaging;

namespace ProductManagement.Application.UseCases.Products.Commands.UpdateProduct;
public class UpdateProductCommandHandler : ICommandHandler<UpdateProductCommand, bool>
{
    private readonly IProductTestDbContext _dbContext;

    public UpdateProductCommandHandler(IProductTestDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<bool> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
    {
        var product = await _dbContext.Products.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        if (product is null) return false;

        product.Name = request.ProductDto.Name;
        product.Description = request.ProductDto.Description;

        int result = await _dbContext.SaveChangesAsync(cancellationToken);
        return result != 0;
    }
}
