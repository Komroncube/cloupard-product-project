﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using ProductManagement.Application.Contracts;
using ProductManagement.Application.Messaging;
using ProductManagement.Domain.Models;

namespace ProductManagement.Application.UseCases.Products.Queries.GetAllProducts;
public class GetAllProductQueryHandler : IQueryHandler<GetAllProductsQuery, IEnumerable<Product>>
{
    private readonly IProductTestDbContext _dbContext;
    public GetAllProductQueryHandler(IProductTestDbContext context)
    {
        _dbContext = context;
    }
    public async Task<IEnumerable<Product>> Handle(GetAllProductsQuery request, CancellationToken cancellationToken)
    {
        return await _dbContext.Products.Where(x => x.Name.Contains(request.ProductName)).ToListAsync(cancellationToken);
    }
}
