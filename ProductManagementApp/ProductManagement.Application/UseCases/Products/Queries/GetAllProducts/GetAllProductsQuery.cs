﻿using MediatR;
using ProductManagement.Application.Messaging;
using ProductManagement.Domain.Models;

namespace ProductManagement.Application.UseCases.Products.Queries.GetAllProducts;
public class GetAllProductsQuery : IQuery<IEnumerable<Product>>
{
    public string ProductName { get; set; }

    public GetAllProductsQuery(string? productName)
    {
        ProductName = productName?.Trim() ?? "";
    }
}
