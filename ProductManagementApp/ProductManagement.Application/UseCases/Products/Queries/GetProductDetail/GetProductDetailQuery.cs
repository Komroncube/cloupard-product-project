﻿using MediatR;
using ProductManagement.Application.Messaging;
using ProductManagement.Domain.Models;

namespace ProductManagement.Application.UseCases.Products.Queries.GetProductDetail;

public class GetProductDetailQuery : IQuery<Product>
{
    public Guid Id { get; set; }

    public GetProductDetailQuery(Guid id)
    {
        Id = id;
    }
}
