﻿using Microsoft.EntityFrameworkCore;
using ProductManagement.Application.Contracts;
using ProductManagement.Application.Messaging;
using ProductManagement.Domain.Models;

namespace ProductManagement.Application.UseCases.Products.Queries.GetProductDetail;
public class GetProductDetailQueryHandler : IQueryHandler<GetProductDetailQuery, Product>
{
    private readonly IProductTestDbContext _dbContext;

    public GetProductDetailQueryHandler(IProductTestDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Product> Handle(GetProductDetailQuery request, CancellationToken cancellationToken)
    {
        var product = await _dbContext.Products.FirstOrDefaultAsync(x => x.Id == request.Id, cancellationToken);
        if (product == null)
        {
            throw new ArgumentException($"Product with id: {request.Id} not found");
        }
        return product;
    }
}
