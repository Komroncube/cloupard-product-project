﻿namespace ProductManagement.Application.DTOs;
public record ProductDTO(string Name, string? Description)
{
}
