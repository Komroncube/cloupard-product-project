﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.MVC.Models;
using ProductManagement.MVC.Services;

namespace ProductManagement.MVC.Controllers;
public class ProductsController : Controller
{
    private readonly HttpClient _client;
    private const string _apiProductsPath = $"api/Products";
    private readonly ILogger<ProductsController> _logger;

    public ProductsController(IClient httpClient, ILogger<ProductsController> logger)
    {
        _logger = logger;
        _client = httpClient.HttpClient;
    }

    public async Task<ActionResult> Index(string searchName)
    {
        var queryString = _apiProductsPath;
        if (!string.IsNullOrEmpty(searchName))
        {
            queryString += $"?productName={searchName.Trim()}";
        } 
        var products = await _client.GetFromJsonAsync<IEnumerable<ProductViewModel>>(queryString) ?? new List<ProductViewModel>();
        return View(products);
    }


    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Create(ProductViewModel product)
    {
        try
        {
            var response = await _client.PostAsJsonAsync(_apiProductsPath, product);
            if (response.IsSuccessStatusCode)
            {
                TempData["success"] = "Product created successfully";
            }
            else
            {
                TempData["error"] = "Product not created";
            }

            return RedirectToAction(nameof(Index));
        }
        catch (Exception ex)
        {
            _logger.LogError(HttpContext.TraceIdentifier, ex);
            return Empty;
        }
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit(ProductViewModel product)
    {
        try
        {
            var queryString = _apiProductsPath + $"?id={product.Id}";
            var response = await _client.PutAsJsonAsync(queryString, product);
            if (response.IsSuccessStatusCode)
            {
                TempData["success"] = "Product updated successfully";
            }
            else
            {
                TempData["error"] = "Product not updated";
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(HttpContext.TraceIdentifier, ex);
        }
        return RedirectToAction(nameof(Index));

    }
    [HttpPost]
    public async Task<ActionResult> Delete(ProductViewModel product)
    {
        try
        {
            var queryString = _apiProductsPath + $"?id={product.Id}";
            var response = await _client.DeleteAsync(queryString);
            if (response.IsSuccessStatusCode)
            {
                TempData["success"] = "Product deleted successfully";
            }
            else
            {
                TempData["error"] = "Product not deleted";
            }

            return RedirectToAction(nameof(Index));

        }
        catch (Exception ex)
        {
            _logger.LogError(HttpContext.TraceIdentifier, ex);
            return Empty;
        }
    }

}
