﻿namespace ProductManagement.MVC.Services;

public interface IClient
{
    public HttpClient HttpClient { get; }
}
