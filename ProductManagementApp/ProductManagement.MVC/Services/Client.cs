﻿namespace ProductManagement.MVC.Services;

public class Client : IClient
{
    public HttpClient HttpClient { get; }

    public Client(HttpClient httpClient)
    {
        HttpClient = httpClient;
    }
}
