﻿using System.ComponentModel.DataAnnotations;

namespace ProductManagement.MVC.Models;

public class ProductViewModel
{
    public Guid Id { get; set; } = Guid.Empty;
    [Required]
    public string Name { get; set; }
    public string? Description { get; set; }
}
