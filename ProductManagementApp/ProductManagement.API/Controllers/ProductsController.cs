﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using ProductManagement.Application.DTOs;
using ProductManagement.Application.UseCases.Products.Commands.CreateProduct;
using ProductManagement.Application.UseCases.Products.Commands.DeleteProduct;
using ProductManagement.Application.UseCases.Products.Commands.UpdateProduct;
using ProductManagement.Application.UseCases.Products.Queries.GetAllProducts;
using ProductManagement.Domain.Models;
using System.Net;
using ProductManagement.Application.UseCases.Products.Queries.GetProductDetail;

namespace ProductManagement.API.Controllers;
[Route("api/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private readonly IMediator _mediator;

    public ProductsController(IMediator mediator)
    {
        _mediator = mediator;
    }

    [HttpGet]
    public async ValueTask<IActionResult> GetProducts(string? productName)
    {
        IEnumerable<Product> products = await _mediator.Send(new GetAllProductsQuery(productName));
        return Ok(products);
    }
    [HttpGet("{id}")]
    public async ValueTask<IActionResult> GetProduct(Guid id)
    {
        Product product = await _mediator.Send(new GetProductDetailQuery(id));
        return Ok(product);
    }

    [HttpPost]
    public async ValueTask<IActionResult> CreateProduct(ProductDTO productDto)
    {
        bool isSuccess = await _mediator.Send(new CreateProductCommand(productDto));
        if (isSuccess)
        {
            return Created(HttpContext.Request.Path.ToString(), isSuccess);
        }

        return BadRequest();
    }

    [HttpPut]
    public async ValueTask<IActionResult> UpdateProduct(Guid id, ProductDTO productDto)
    {
        try
        {
            bool isSuccess = await _mediator.Send(new UpdateProductCommand(id, productDto));
            if (isSuccess)
            {
                return Ok();
            }
            return BadRequest();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);

            HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            throw;
        }
    }

    [HttpDelete]
    public async ValueTask<IActionResult> DeleteProduct(Guid id)
    {
        bool isSuccess = await _mediator.Send(new DeleteProductCommand(id));
        if (isSuccess)
            return NoContent();
        return BadRequest();
    }
}
